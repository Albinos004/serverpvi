const express = require('express')
const http = require('http')
const mongoose = require('mongoose');
const Message = require("./models/message");
const {getRoomUsers, userJoin, getCurrentUser, userLeave} = require("./users");
const {formatMessage, formatHistoryMessage} = require("./messages");
const app = express()
const server = http.createServer(app)

const socketio = require('socket.io')(server, {
    cors: {
        origin: '*',
    }
});

mongoose.connect('mongodb://127.0.0.1:27017/Chat', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Connected to MongoDB'))
    .catch((err) => console.error('Error connecting to MongoDB', err));

let currentUsers = []

//Run when client connects
socketio.on('connection', socket => {
    console.log('New WS connected')
    let Username = "Guest"

    socket.on("login", username => {
        Username = username

        for (let i = 0; i < currentUsers.length; i++){
            socket.emit("createDirectMessageRoom", currentUsers[i])
        }

        currentUsers.push(Username)
        socket.broadcast.emit("createDirectMessageRoom", Username)
    })

    socket.on('leaveCurrentRoom', () => {
        const user = userLeave(socket.id);

        if (user) {
            // Send users and room info
            socketio.to(user.room).emit("roomUsers", {
                room: user.room,
                users: getRoomUsers(user.room),
            });
        }
    })

    socket.on("joinDirect", room => {
        console.log("Entered direct room")
        const roomID = generatePrivateRoomID(Username, room)

        const user = userJoin(socket.id, Username, roomID)

        socket.join(roomID)

        socketio.to(roomID).emit("roomUsers", {
            room: roomID,
            users: getRoomUsers(roomID)
        })

        Message.find({ room: roomID })
            .then(messages => {
                messages.forEach(message => {
                    socket.emit("historyMessage",formatHistoryMessage(message.author, message.text, message.time));
                });
            })
            .catch(err => {
                console.error(err);
            });
    })

    socket.on("joinRoom", room => {
        const user = userJoin(socket.id, Username, room)

        socket.join(user.room)

        socketio.to(user.room).emit("roomUsers", {
            room: user.room,
            users: getRoomUsers(user.room)
        })

        Message.find({ room: user.room })
            .then(messages => {
                messages.forEach(message => {
                    socket.emit("historyMessage",formatHistoryMessage(message.author, message.text, message.time));
                });
            })
            .catch(err => {
                console.error(err);
            });
    })

    socket.on("chatMessage", (msg) => {
        const user = getCurrentUser(socket.id);
        socketio.to(user.room).emit("message", formatMessage(user.username, msg, user.room));
    })

    socket.on("disconnect", () => {
        const user = userLeave(socket.id);

        if (user) {
            // Send users and room info
            socketio.to(user.room).emit("roomUsers", {
                room: user.room,
                users: getRoomUsers(user.room),
            });
        }
    })
})

const PORT = 3000;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`))

function generatePrivateRoomID(username1, username2) {
    // Sort the usernames alphabetically to ensure consistent room ID
    const sortedUsernames = [username1, username2].sort();

    // Generate a unique room ID by combining sorted usernames
    const roomID = `private_${sortedUsernames[0]}_${sortedUsernames[1]}`;

    return roomID;
}