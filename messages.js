const moment = require('moment');
const Message = require("./models/message");

function formatMessage(username, text, room) {
    const newMessage = new Message({
        text: text,
        author: username,
        time: moment().format('h:mm a'),
        room: room
    })

    newMessage.save()
        .then((message) => {
            console.log(`Message saved to messages collection: ${message}`);
        })
        .catch((err) => {
            console.error(err);
        });

    return {
        username,
        text,
        time: moment().format('h:mm a')
    };
}

function formatHistoryMessage(username, text, time) {
    return {
        username,
        text,
        time: time
    };
}

module.exports = {formatMessage, formatHistoryMessage}